package com.training.demochampion.controller;

import com.training.demochampion.entities.LolChampion;
import com.training.demochampion.service.LolChampionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/api/v1/lolchampion")
public class ChampionController {

    @Autowired
    private LolChampionService lolChampionService;

    @GetMapping
    public List<LolChampion> findAll() {
        return lolChampionService.findAll();
    }

    @PostMapping
    public LolChampion save(LolChampion lolChampion) {
        return lolChampionService.save(lolChampion);
    }

    @GetMapping("{id}")
    public LolChampion findById(@PathVariable long id) {
        return lolChampionService.findById(id);
    }
}
