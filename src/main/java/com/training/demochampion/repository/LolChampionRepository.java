package com.training.demochampion.repository;

import com.training.demochampion.entities.LolChampion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LolChampionRepository extends JpaRepository<LolChampion,Long> {

}
