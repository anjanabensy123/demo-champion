package com.training.demochampion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoChampionApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoChampionApplication.class, args);
    }

}
