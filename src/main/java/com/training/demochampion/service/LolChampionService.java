package com.training.demochampion.service;

import com.training.demochampion.entities.LolChampion;
import com.training.demochampion.repository.LolChampionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class LolChampionService {

    @Autowired
    LolChampionRepository lolChampionRepository;

    public List<LolChampion> findAll(){
        return lolChampionRepository.findAll();
    }
    public LolChampion save(LolChampion lolChampion){
        return lolChampionRepository.save(lolChampion);
    }
    public LolChampion findById(long id){
        return lolChampionRepository.findById(id).get();
    }
}
